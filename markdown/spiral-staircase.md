---
date: 2024-12-12
title: Spiral Staircase
---
#### A Hobbit's Guide to Stairs

1.	A Time-machine in the Garden
2.	One Wednesday in 1936


#### Elf-tower

3. [Frodo's Dreams](https://www.silmarillionwritersguild.org/node/7361)
4. [Seeing Stones in Dark Towers](https://www.silmarillionwritersguild.org/node/7422)
5. [Crossroads](https://www.silmarillionwritersguild.org/node/7495)
6. [Thálatta! Thálatta!](https://www.silmarillionwritersguild.org/node/7575)


#### Anglo-Saxon Tower

7. [Passing Ships](https://www.silmarillionwritersguild.org/node/7675)
8. [Straight Road](https://www.silmarillionwritersguild.org/node/7766)
9. [Spiral Staircase](https://www.silmarillionwritersguild.org/node/7811)
10. [Doom and Ascent](https://www.silmarillionwritersguild.org/node/7867)


#### Dark Tower

11.	Ghost Sequel
12.	Third Age
13.	Sea-road
14. Fairy-tale Turn


#### Epilogue. Undertowers: Stone and Book


![](https://yemachine.com/images/fusion.png)


Initial drafts of chapters 3-10 published on the [Silmarillion Writers Guild](https://www.silmarillionwritersguild.org/), in their newsletter column [A Sense of History](https://www.silmarillionwritersguild.org/sense-of-history).
